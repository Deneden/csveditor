using System;
using Domain.Models;
using Infrastructure.Services;
using Xunit;

namespace Tests
{
    public class CsvParserTests
    {
        [Fact]
        public void ParseNameTest()
        {
            string[] names =
            {
                "������ ���� ��������",
                "������ ���� ��������",
                "������ ���� ��������",
                "������ ���� ��������"
            };

            string[] expected =
            {
                "������",
                "����",
                "��������"
            };

            CsvParser parser = new CsvParser();

            Assert.All(names, x=> Assert.Equal(expected,parser.ParseName(x)));
        }

        [Fact]
        public void ParsePhoneTest()
        {
            string[] phones =
            {
                "+79998886655",
                "8 999 888 66 55",
                "8-999-888-66-55",
                "8 (999) 888-66-55",
                "+7 999 888 6655",
                "999 888 6655",
                "999-888-6655"
            };

            CsvParser parser = new CsvParser();

            Assert.All(phones, x=> Assert.Equal("9998886655", parser.ParsePhoneNumber(x)));
        }

        [Fact]
        public void ParseDate()
        {
            string[] dates =
            {
                "01.01.2001",
                "01-01-2001",
                "01 01 2001",
                "01/01/2001",
                "01 ������ 2001",
                "2001.01.01",
                "2001-01-01",
                "2001/01/01"
            };

            DateTime expected = new DateTime(2001,01,01);
            
            CsvParser parser = new CsvParser();

            Assert.All(dates, x=> Assert.Equal(expected, parser.ParseDate(x)));
        }

        [Fact]
        public void ParseEntityTest()
        {
            string[] entities =
            {
                "������� ���� ����������\t29 ���� 2975\tturanga@mail.com\t+76585684445",
                "������� ������ ��������\t07.11.1967\tgribkov@mail.ru\t+79156594585",
                "��������� ������� ����������\t05.03.1978\tpolosatov@mail.com\t89156258546"
            };

            CsvParser parser = new CsvParser();

            Assert.All(entities, x=>Assert.IsType<Worksheet>(parser.Parse(x,'\t')));
        }

        [Fact]
        public void IsNullTest()
        {
            string[] errorStrings =
            {
                "",
                "foo",
                "foo\tbar",
                "foo\tbar\tbaz"
            };

            CsvParser parser = new CsvParser();

            Assert.All(errorStrings, x=> Assert.Null(parser.Parse(x, '\t')));
        }

    }
}
