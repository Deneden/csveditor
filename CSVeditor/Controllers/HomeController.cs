﻿using Microsoft.AspNetCore.Mvc;

namespace Presentation.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        /// Возвращает представление главной страницы.
        /// </summary>
        /// <returns>Представление Razor</returns>
        public IActionResult Index() => View();
    }
}