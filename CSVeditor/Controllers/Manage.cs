﻿using System.Collections.Generic;
using System.Linq;
using Domain.Models;
using Infrastructure.Interfaces;
using Infrastructure.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Presentation.Controllers
{
    [Route("api/[controller]")]
    public class Manage : Controller
    {
        private readonly IUnitOfWork _uow;

        //количество результатов на странице.
        private readonly int _pageSize;

        /// <summary>
        /// Конструктор класса.
        /// </summary>
        /// <param name="uow">Unit of Work</param>
        /// <param name="configuration">параметры конфигурации</param>
        public Manage(IUnitOfWork uow, IOptions<AppConfig> configuration)
        {
            _uow = uow;
            _pageSize = configuration.Value.PageSize;
            //для демонстрации вносим данные сразу при инициализации. можно удалить.
            //if (_factory.GetWorksheetsCount()<=0)
            //{
            //    _factory.AddWorksheet(new Worksheet(phoneNumber: "9187205566", bornDate: DateTime.Parse("1993-10-05"), name: "Антон", surname: "Антошкин", secondName: "Антонович", email: "a.antoshkin@mail.com"));
            //    _factory.AddWorksheet(new Worksheet(phoneNumber: "9135196688", bornDate: DateTime.Parse("1990-10-05"), name: "Петручо", surname: "Петручин", secondName: "Петручевич", email: "p.petruchin@mail.com"));
            //    _factory.SaveChanges();
            //}
        }

        /// <summary>
        /// Возвращает кандидата по id.
        /// </summary>
        /// <param name="id">Id кандидата</param>
        /// <returns>ObjectResult</returns>
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            Worksheet worksheet = _uow.WorksheetQuery.FindWorksheetByIdQuery(id);
            if (worksheet == null) return NotFound();
            return new ObjectResult(worksheet);
        }

        /// <summary>
        /// Получение списка кандидатов с сортировкой и пагинацией
        /// </summary>
        /// <returns>Коллекция кандидатов</returns>
        [HttpGet]
        public IEnumerable<Worksheet> Get(string sort = "id", int page = 1)
        {
            return _uow.WorksheetQuery.GetWorksheetsOrderBy(sort).Skip((page - 1) * _pageSize).Take(_pageSize).ToList();
        }

        /// <summary>
        /// Получение количества кандидатов и количества страниц для пагинации
        /// </summary>
        /// <returns>ObjectResult</returns>
        [HttpGet("total")]
        public IActionResult Total()
        {
            IPagination page = new Pagination
            {
                TotalItems = _uow.WorksheetQuery.GetWorksheetsCount(),
                ItemsPerPage = _pageSize
            };
            return new ObjectResult(page);
        }

        /// <summary>
        /// Добавление нового кандидата
        /// </summary>
        /// <returns>ObjectResult Ok</returns>
        [HttpPost]
        public IActionResult Post([FromBody] Worksheet worksheet)
        {
            if (worksheet == null) return BadRequest();
            _uow.WorksheetQuery.AddWorksheet(worksheet);
            _uow.Save();

            return Ok(worksheet);
        }

        /// <summary>
        /// Изменение кандидата
        /// </summary>
        /// <param name="worksheet">Кандидат на изменение</param>
        /// <returns>ObjectResult Ok</returns>
        [HttpPut]
        public IActionResult Put([FromBody] Worksheet worksheet)
        {
            if (worksheet == null) return BadRequest();
            if (!_uow.WorksheetQuery.IsWorksheetExist(worksheet.Id)) return NotFound();

            _uow.WorksheetQuery.UpdateWorksheet(worksheet);
            _uow.Save();

            return Ok(worksheet);
        }

        /// <summary>
        /// Удаление кандидата
        /// </summary>
        /// <param name="id">Id кандидата</param>
        /// <returns>ObjectResult Ok</returns>
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            Worksheet worksheet = _uow.WorksheetQuery.FindWorksheetByIdQuery(id);
            if (worksheet == null) return NotFound();

            _uow.WorksheetQuery.DeleteWorksheet(worksheet);
            _uow.Save();

            return Ok(worksheet);
        }
    }
}
