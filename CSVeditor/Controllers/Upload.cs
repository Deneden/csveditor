﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Interfaces;
using Domain.Models;
using Infrastructure.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Presentation.Controllers
{
    [Route("api/[controller]")]
    public class Upload : Controller
    {
        private readonly IUnitOfWork _uow;
        private readonly IParser<IEntity> _parser;

        /// <summary>
        /// Конструктор класса.
        /// </summary>
        /// <param name="uow">Unit of Work</param>
        /// <param name="parser">Парсер</param>
        public Upload(IUnitOfWork uow, IParser<IEntity> parser)
        {
            _uow = uow;
            _parser = parser;
        }

        /// <summary>
        /// Возвращает файл CSV с данными из таблицы.
        /// </summary>
        /// <returns>OK результат</returns>
        [HttpGet]
        [Route("data.csv")]
        [Produces("text/csv")]
        public IActionResult GetCsv() => Ok(_uow.WorksheetQuery.GetAllWorksheets().ToList());

        /// <summary>
        /// Получает CSV файл для дальнейшего парсинга.
        /// </summary>
        /// <param name="file">файл для парсинга</param>
        /// <returns>Перенаправление на Home->Index</returns>
        [HttpPost]
        public async Task<IActionResult> Post(IFormFile file)
        {
            IEnumerable<IEntity> entities = await _parser.ParseCollection(file);

            if (entities != null)
            {
                foreach (var entity in entities)
                {
                    _uow.WorksheetQuery.AddWorksheet(entity as Worksheet);                   
                }
                _uow.Save();
            }
            else return BadRequest();

            return RedirectToAction("Index", "Home");
        }
    }
}
