﻿//глобальные переменные
var _sort = "id";
var _page = 1;

//Получить список
function GetWorksheets(sort, page) {
    $.ajax({
        url: '/api/manage?sort=' + sort + '&page=' + page,
        type: 'GET',
        contentType: "application/json",
        success: function (worksheets) {
            var rows = "";
            $.each(worksheets, function (index, worksheet) {
                rows += row(worksheet);
            });
            $("table tbody").empty();
            $("table tbody").append(rows);
        }
    });
}

//Получить одну запись по id
function GetWorksheet(id) {
    $.ajax({
        url: '/api/manage/' + id,
        type: 'GET',
        contentType: "application/json",
        success: function (worksheet) {
            var form = document.forms["worksheetForm"];
            form.elements["id"].value = worksheet.id;
            form.elements["name"].value = worksheet.name;
            form.elements["surname"].value = worksheet.surname;
            form.elements["secondName"].value = worksheet.secondName;
            form.elements["bornDate"].value = worksheet.bornDate;
            form.elements["email"].value = worksheet.email;
            form.elements["phoneNumber"].value = worksheet.phoneNumber;
        }
    });
}

//Создание новой записи
function CreateUser(wsName, wsSurname, wsSecondName, wsBornDate, wsEmail, wsPhoneNumber) {
    $.ajax({
        url: "api/manage",
        contentType: "application/json",
        method: "POST",
        data: JSON.stringify({
            name: wsName,
            surname: wsSurname,
            secondName: wsSecondName,
            bornDate: wsBornDate,
            email: wsEmail,
            phoneNumber: wsPhoneNumber
        }),
        success: function (worksheet) {
            reset();
            $("table tbody").append(row(worksheet));
        }
    });
}

//Редактирование существующей записи
function EditUser(wsId, wsName, wsSurname, wsSecondName, wsBornDate, wsEmail, wsPhoneNumber) {
    $.ajax({
        url: "api/manage",
        contentType: "application/json",
        method: "PUT",
        data: JSON.stringify({
            id: wsId,
            name: wsName,
            surname: wsSurname,
            secondName: wsSecondName,
            bornDate: wsBornDate,
            email: wsEmail,
            phoneNumber: wsPhoneNumber
        }),
        success: function (worksheet) {
            reset();
            $("tr[data-rowid='" + worksheet.id + "']").replaceWith(row(worksheet));
        }
    });
}

//Очистка формы
function reset() {
    var form = document.forms["worksheetForm"];
    form.reset();
    form.elements["id"].value = 0;
}

//Удаление записи
function DeleteWorksheet(id) {
    $.ajax({
        url: "api/manage/" + id,
        contentType: "application/json",
        method: "DELETE",
        success: function (worksheet) {
            $("tr[data-rowid='" + worksheet.id + "']").remove();
        }
    });
}

//Генерация строки в таблице
var row = function (worksheet) {
    return "<tr data-rowid='" + worksheet.id + "'>" +
        "<td>" + worksheet.id + "</td>" +
        "<td>" + worksheet.surname + "</td> " +
        "<td>" + worksheet.name + "</td> " +
        "<td>" + worksheet.secondName + "</td> " +
        "<td>" + worksheet.bornDate + "</td> " +
        "<td>" + worksheet.email + "</td> " +
        "<td>" + worksheet.phoneNumber + "</td>" +
        "<td><a class='editLink' data-id='" + worksheet.id + "'>Изменить</a> | " +
        "<a class='removeLink' data-id='" + worksheet.id + "'>Удалить</a></td>" +
        "</tr>";
};

//Обработка нажатия на кнопку очистки формы
$("#reset").click(function (e) {
    e.preventDefault();
    reset();
});

//Обработка нажатия на кнопку сохранения формы
$("#worksheetForm").submit(function (e) {
    e.preventDefault();
    var id = this.elements["id"].value;
    var name = this.elements["name"].value;
    var surname = this.elements["surname"].value;
    var secondName = this.elements["secondName"].value;
    var bornDate = this.elements["bornDate"].value;
    var email = this.elements["email"].value;
    var phoneNumber = this.elements["phoneNumber"].value;
    if (id == 0)
        CreateUser(name, surname, secondName, bornDate, email, phoneNumber);
    else
        EditUser(id, name, surname, secondName, bornDate, email, phoneNumber);
    $("#modalWindow").modal("hide");
});

//Обработка нажатия на кнопку редактирования
$("body").on("click", ".editLink", function () {
    var id = $(this).data("id");
    GetWorksheet(id);
    $("#modalWindow").modal("show");
});

//Обработка нажатия на кнопку удаления
$("body").on("click", ".removeLink", function () {
    var id = $(this).data("id");
    DeleteWorksheet(id);
});

//Обработка нажатия на кнопку сортировки
$("#tableId").click(function () {
    _sort = "id";
    GetWorksheets(_sort, _page);
});

$("#tableSurname").click(function () {
    _sort = "surname";
    GetWorksheets(_sort, _page);
});

$("#tableName").click(function () {
    _sort = "name";
    GetWorksheets(_sort, _page);
});

$("#tableSecondName").click(function () {
    _sort = "secondname";
    GetWorksheets(_sort, _page);
});

$("#tableDate").click(function () {
    _sort = "date";
    GetWorksheets(_sort, _page);
});

$("#tableEmail").click(function () {
    _sort = "email";
    GetWorksheets(_sort, _page);
});

$("#tablePhone").click(function () {
    _sort = "phone";
    GetWorksheets(_sort, _page);
});

//Пагинация
function GetPagination() {
    $.ajax({
        url: '/api/manage/total',
        type: 'GET',
        contentType: "application/json",
        success: function (page) {
            for (var i = 1; i <= page.totalPages; i++) {
                $(".pagination").append('<li><a class="pageSelector" href="#">' + i + '</a></li>');
            }
        }
    });
}

//Обработка пагинации
$("body").on("click", ".pageSelector", function () {
    _page = this.innerHTML;
    GetWorksheets(_sort, _page);
});


GetWorksheets();
GetPagination()