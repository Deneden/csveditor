﻿using System.Text;
using Domain.Interfaces;
using Infrastructure.Context;
using Infrastructure.Interfaces;
using Infrastructure.Models;
using Infrastructure.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WebApiContrib.Core.Formatter.Csv;

namespace Presentation
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            string connection = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<ApplicationContext>(options => options.UseSqlServer(connection));

            var csvFormatterOptions = new CsvFormatterOptions
            {
                Encoding = Encoding.UTF8.EncodingName
            };

            services.Configure<AppConfig>(Configuration.GetSection("AppConfig"));
            services.AddMvc(opt =>
                {
                    opt.OutputFormatters.Add(new CsvOutputFormatter(csvFormatterOptions));
                })
                .AddJsonOptions(
                    opt =>
                    {
                        opt.SerializerSettings.DateFormatString = "dd.MM.yyyy";
                    });

            services.AddTransient<ApplicationContext>();
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddTransient<IParser<IEntity>, CsvParser>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

            }

            app.UseStatusCodePages();
            app.UseStaticFiles();
            app.UseMvcWithDefaultRoute();
        }
    }
}
