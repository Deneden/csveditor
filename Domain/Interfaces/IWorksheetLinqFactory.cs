﻿using System.Collections.Generic;
using Domain.Models;

namespace Domain.Interfaces
{
    public interface IWorksheetLinqFactory
    {
        /// <summary>
        /// Возвращает Worksheet по Id
        /// </summary>
        /// <param name="id">Id сущности</param>
        /// <returns>Сущность Worksheet</returns>
        Worksheet FindWorksheetByIdQuery(int id);

        /// <summary>
        /// Возвращает коллекцию Worksheet
        /// </summary>
        /// <returns>Коллекция Worksheet</returns>
        IEnumerable<Worksheet> GetAllWorksheets();

        /// <summary>
        /// Возвращает коллекцию сущностей сортированных по критерию order
        /// </summary>
        /// <returns>Коллекция сущностей Worksheet</returns>
        IEnumerable<Worksheet> GetWorksheetsOrderBy(string order);

        /// <summary>
        /// Добавляет Worksheet в репозиторий
        /// </summary>
        /// <param name="worksheet">Сущность Worksheet</param>
        void AddWorksheet(Worksheet worksheet);

        /// <summary>
        /// Возвращает количество сущностей
        /// </summary>
        /// <returns>Количество сущностей</returns>
        int GetWorksheetsCount();

        /// <summary>
        /// Изменяет Worksheet в базе данных
        /// </summary>
        /// <param name="worksheet">Сущность Worksheet</param>
        void UpdateWorksheet(Worksheet worksheet);

        /// <summary>
        /// Удаляет сущность Worksheet
        /// </summary>
        /// <param name="worksheet">Сущность Worksheet</param>
        void DeleteWorksheet(Worksheet worksheet);

        /// <summary>
        /// Проверяет наличие сущности в базе по Id
        /// </summary>
        /// <param name="id">Id сущности</param>
        /// <returns>True если есть, иначе false</returns>
        bool IsWorksheetExist(int id);
  
    }
}
