﻿namespace Domain.Interfaces
{
    public interface IEntity
    {
        /// <summary>
        /// Возвращает ID сущности.
        /// </summary>
        /// <returns>Возвращает ID</returns>
        int GetId();
    }
}
