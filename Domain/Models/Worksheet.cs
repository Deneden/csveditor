﻿using System;
using System.ComponentModel.DataAnnotations;
using Domain.Interfaces;

namespace Domain.Models
{
    //основной класс модели
    public class Worksheet : IEntity
    {
        [Key, Required]
        public int Id { get; set; }

        public string Name { get; set; }
        public string Surname { get; set; }
        public string SecondName { get; set; }
        public DateTime BornDate { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }

        [Obsolete("Для связывания модели и Entity Framework")]
        internal Worksheet()
        {
        }

        public Worksheet(
            string phoneNumber = null,
            DateTime bornDate = default(DateTime),
            string name = null,
            string surname = null,
            string secondName = null,
            string email = null)
        {
            PhoneNumber = phoneNumber;
            Name = name;
            Surname = surname;
            SecondName = secondName;
            BornDate = bornDate;
            Email = email;
        }

        public int GetId() => Id;
    }
}
