﻿using System.Collections.Generic;
using Domain.Models;
using Infrastructure.Interfaces;

namespace Infrastructure.Linq
{
    public class GetAllWorksheets
    {
        private readonly ILinqProvider<Worksheet> _repository;

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="repository">Репозиторий</param>
        public GetAllWorksheets(ILinqProvider<Worksheet> repository) => _repository = repository;

        /// <summary>
        /// Получает все сущности
        /// </summary>
        /// <returns>Коллекция Worksheet</returns>
        public IEnumerable<Worksheet> Execute() => _repository.Query();
    }
}
