﻿using Domain.Models;
using Infrastructure.Interfaces;

namespace Infrastructure.Linq
{
    public class DeleteWorksheet
    {
        private readonly ILinqProvider<Worksheet> _repository;
        private readonly Worksheet _worksheet;

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="repository">Репозиторий</param>
        /// <param name="worksheet">сущность Worksheet</param>
        public DeleteWorksheet(ILinqProvider<Worksheet> repository, Worksheet worksheet)
        {
            _repository = repository;
            _worksheet = worksheet;
        }

        /// <summary>
        /// Удаляет сущность Worksheet
        /// </summary>
        public void Execute() => _repository.Remove(_worksheet);
    }
}
