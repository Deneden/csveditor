﻿using Domain.Models;
using Infrastructure.Interfaces;

namespace Infrastructure.Linq
{
    public class AddWorksheet
    {
        private readonly ILinqProvider<Worksheet> _repository;
        private readonly Worksheet _worksheet;

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="repository">Репозиторий</param>
        /// /// <param name="worksheet">Сущность для добавления</param>
        public AddWorksheet(ILinqProvider<Worksheet> repository, Worksheet worksheet)
        {
            _repository = repository;
            _worksheet = worksheet;
        }

        /// <summary>
        /// Добавляет Worksheet в репозиторий
        /// </summary>
        /// <returns>Сущность Worksheet</returns>
        public void Execute() => _repository.Add(_worksheet);
    }
}
