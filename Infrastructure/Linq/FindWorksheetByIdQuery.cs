﻿using Domain.Models;
using Infrastructure.Interfaces;

namespace Infrastructure.Linq
{
    public class FindWorksheetByIdQuery
    {
        private readonly ILinqProvider<Worksheet> _repository;
        private readonly int _id;

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="repository">Репозиторий</param>
        /// <param name="id">Id сущности</param>
        public FindWorksheetByIdQuery(ILinqProvider<Worksheet> repository,
            int id)
        {
            _repository = repository;
            _id = id;
        }

        /// <summary>
        /// Получает сущность по Id
        /// </summary>
        /// <returns>Сущность Worksheet</returns>
        public Worksheet Execute() => _repository.GetById(_id);
    }
}
