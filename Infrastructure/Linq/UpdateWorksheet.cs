﻿using Domain.Models;
using Infrastructure.Interfaces;

namespace Infrastructure.Linq
{
    public class UpdateWorksheet
    {
        private readonly ILinqProvider<Worksheet> _repository;
        private readonly Worksheet _worksheet;

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="repository">Репозиторий</param>
        /// <param name="worksheet">сущность Worksheet</param>
        public UpdateWorksheet(ILinqProvider<Worksheet> repository, Worksheet worksheet)
        {
            _repository = repository;
            _worksheet = worksheet;
        }

        /// <summary>
        /// Изменяет сущность Worksheet
        /// </summary>
        public void Execute()
        {
            _repository.Update(_worksheet);
        }
    }
}
