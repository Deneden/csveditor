﻿using System.Linq;
using Domain.Models;
using Infrastructure.Interfaces;

namespace Infrastructure.Linq
{
    public class IsWorksheetExist
    {
        private readonly ILinqProvider<Worksheet> _repository;
        private readonly int _id;

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="repository">Репозиторий</param>
        /// <param name="id">Id сущности</param>
        public IsWorksheetExist(ILinqProvider<Worksheet> repository, int id)
        {
            _repository = repository;
            _id = id;
        }

        /// <summary>
        /// Проверяет наличие сущности в базе по Id
        /// </summary>
        /// <returns>True если есть, иначе false</returns>
        public bool Execute() => _repository.Query().Any(x => x.Id == _id);
    }
}
