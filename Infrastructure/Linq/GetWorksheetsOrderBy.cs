﻿using System.Collections.Generic;
using System.Linq;
using Domain.Models;
using Infrastructure.Interfaces;

namespace Infrastructure.Linq
{
    public class GetWorksheetsOrderBy
    {
        private readonly ILinqProvider<Worksheet> _repository;
        private readonly string _order;

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="repository">Репозиторий</param>
        /// <param name="order">Критерий сортировки</param>
        public GetWorksheetsOrderBy(ILinqProvider<Worksheet> repository,
            string order)
        {
            _repository = repository;
            _order = order;
        }

        /// <summary>
        /// Возвращает коллекцию сущностей сортированных по критерию order
        /// </summary>
        /// <returns>Коллекция сущностей Worksheet</returns>
        public IEnumerable<Worksheet> Execute()
        {
            switch (_order)
            {
                case "id": return _repository.Query().OrderBy(x => x.Id);
                case "date": return _repository.Query().OrderBy(x => x.BornDate);
                case "email": return _repository.Query().OrderBy(x => x.Email);
                case "name": return _repository.Query().OrderBy(x => x.Name);
                case "phone": return _repository.Query().OrderBy(x => x.PhoneNumber);
                case "secondname": return _repository.Query().OrderBy(x => x.SecondName);
                case "surname": return _repository.Query().OrderBy(x => x.Surname);
                default: return _repository.Query().OrderBy(x => x.Id);
            }
        }
    }
}
