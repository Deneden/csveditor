﻿using System.Linq;
using Domain.Models;
using Infrastructure.Interfaces;

namespace Infrastructure.Linq
{
    public class GetWorksheetsCount
    {
        private readonly ILinqProvider<Worksheet> _repository;

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="repository">Репозиторий</param>
        public GetWorksheetsCount(ILinqProvider<Worksheet> repository) => _repository = repository;

        /// <summary>
        /// Возвращает количество сущностей
        /// </summary>
        /// <returns>Количество сущностей</returns>
        public int Execute() => _repository.Query().Count();
    }
}
