﻿using Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Context
{
    //контекст данных
    public class ApplicationContext : DbContext
    {
        public DbSet<Worksheet> Worksheets { get; set; }

        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        {       
        }
    }
}
