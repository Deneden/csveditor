﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Infrastructure.Interfaces
{
    public interface IParser<T>
    {
        /// <summary>
        /// Производит структурный разбор строки.
        /// </summary>
        /// <param name="parseString">Строка для разбора</param>
        /// <param name="separator">Разделитель</param>
        /// <returns>Возвращает сущность</returns>
        T Parse(string parseString, char separator);

        /// <summary>
        /// Производит структурный разбор файла.
        /// </summary>
        /// <param name="file">файл для парсинга</param>
        /// <returns>Возвращает коллекцию сущностей</returns>
        Task<IEnumerable<T>> ParseCollection(IFormFile file);

        /// <summary>
        /// Производит разбиение ФИО на отдельные элементы и делает первую букву у каждого заглавной.
        /// </summary>
        /// <param name="data">Строка для разбора</param>
        /// <returns>Возвращает массив строк срдержащих отдельно Фамилию Имя и Отчество</returns>
        string[] ParseName(string data);

        /// <summary>
        /// Производит проверку номера телефона удаляя все символы кроме цифр и обрезая длину до 10 знаков с конца.
        /// </summary>
        /// <param name="data">Строка для разбора</param>
        /// <returns>Возвращает номер телефона</returns>
        string ParsePhoneNumber(string data);

        /// <summary>
        /// Производит парсинг строки в формат DataTime, при неудачном парсинге возвращает минимальне значение даты.
        /// </summary>
        /// <param name="data">Строка для разбора</param>
        /// <returns>Возвращает дату в формате DateTime</returns>
        DateTime ParseDate(string data);

        /// <summary>
        /// Производит парсинг строки с e-mail
        /// </summary>
        /// <param name="data">Строка для разбора</param>
        /// <returns>Возвращает e-mail</returns>
        string ParseEmail(string data);
    }
}
