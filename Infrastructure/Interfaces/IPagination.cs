﻿namespace Infrastructure.Interfaces
{
    public interface IPagination
    {
        /// <summary>
        /// Количество записей всего.
        /// </summary>
        int TotalItems { get; set; }

        /// <summary>
        /// Количество записей на одной странице.
        /// </summary>
        int ItemsPerPage { get; set; }

        /// <summary>
        /// Всего страниц.
        /// </summary>
        int TotalPages { get; }
    }
}
