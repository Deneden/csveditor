﻿using System.Linq;

namespace Infrastructure.Interfaces
{
    public interface ILinqProvider<TEntity> where TEntity : class
    {
        /// <summary>
        /// Возвращает сущность по Id.
        /// </summary>
        /// <param name="id">Id сущности</param>
        /// <returns>Сущность TEntity</returns>
        TEntity GetById(int id);

        /// <summary>
        /// Получает список сущностей из запроса.
        /// </summary>
        /// <returns>IQueryable сущности</returns>
        IQueryable<TEntity> Query();

        /// <summary>
        /// Добавляет сущность в базу данных.
        /// </summary>
        /// <param name="entity">Сущность</param>
        void Add(TEntity entity);

        /// <summary>
        /// Удаляет сущность из базы данных.
        /// </summary>
        /// <param name="entity">Сущность для удаления</param>
        void Remove(TEntity entity);

        /// <summary>
        /// Изменяет сущность в базе данных
        /// </summary>
        /// <param name="entity">Сущность для изменения</param>
        void Update(TEntity entity);
    }
}