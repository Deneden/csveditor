﻿using System;
using Domain.Interfaces;

namespace Infrastructure.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        /// <summary>
        /// Возвращает фабрику запросов для сущности Worksheet.
        /// </summary>
        IWorksheetLinqFactory WorksheetQuery { get; }

        /// <summary>
        /// Сохрняет изменения в базе данных.
        /// </summary>
        void Save();
    }
}