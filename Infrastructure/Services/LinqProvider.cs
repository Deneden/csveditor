﻿using System.Linq;
using Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Services
{
    public class LinqProvider<TEntity> : ILinqProvider<TEntity> where TEntity : class
    {
        private readonly DbSet<TEntity> _dbSet;

        /// <summary>
        /// Конструктор класса.
        /// </summary>
        /// <param name="context">Контекст базы данных</param>
        public LinqProvider(DbContext context) => _dbSet = context.Set<TEntity>();

        /// <summary>
        /// Возвращает сущность по Id.
        /// </summary>
        /// <param name="id">Id сущности</param>
        /// <returns>Сущность TEntity</returns>
        public TEntity GetById(int id) => _dbSet.Find(id);

        /// <summary>
        /// Получает список сущностей из запроса.
        /// </summary>
        /// <returns>IQueryable сущности</returns>
        public IQueryable<TEntity> Query() => _dbSet.AsNoTracking();

        /// <summary>
        /// Добавляет сущность в базу данных.
        /// </summary>
        /// <param name="entity">Сущность</param>
        public void Add(TEntity entity) => _dbSet.Add(entity);

        /// <summary>
        /// Удаляет сущность из базы данных.
        /// </summary>
        /// <param name="entity">Сущность для удаления</param>
        public void Remove(TEntity entity) => _dbSet.Remove(entity);

        /// <summary>
        /// Изменяет сущность в базе данных
        /// </summary>
        /// <param name="entity">Сущность для изменения</param>
        public void Update(TEntity entity) => _dbSet.Update(entity);
    }
}
