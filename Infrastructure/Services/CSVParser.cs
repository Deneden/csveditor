﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Interfaces;
using Domain.Models;
using Infrastructure.Interfaces;
using Microsoft.AspNetCore.Http;

namespace Infrastructure.Services
{
    //парсер csv, без фанатизма
    public class CsvParser : IParser<IEntity>
    {
        /// <summary>
        /// Производит структурный разбор строки.
        /// </summary>
        /// <param name="parseString">Строка для разбора</param>
        /// <param name="separator">Разделитель</param>
        /// <returns>Возвращает сущность</returns>
        public IEntity Parse(string parseString, char separator)
        {
            if (parseString == null) return null;

            string[] data = parseString.Split(separator);

            if (data.Length<4 || data[0] == "ФИО" || data[0] == null || data[1] == null || data[2] == null || data[3] == null) return null;

            string[] fullName = ParseName(data[0]);

            DateTime birthDate = ParseDate(data[1]);

            string email = ParseEmail(data[2]);

            string phone = ParsePhoneNumber(data[3]);

            IEntity worksheet = new Worksheet(phone, birthDate, fullName[1], fullName[0], fullName[2], email);

            return worksheet;
        }

        /// <summary>
        /// Производит структурный разбор файла.
        /// </summary>
        /// <param name="file">файл для парсинга</param>
        /// <returns>Возвращает коллекцию сущностей</returns>
        public async Task<IEnumerable<IEntity>> ParseCollection(IFormFile file)
        {
            if (file != null)
            {
                IList<IEntity> entities = new List<IEntity>();

                try
                {
                    using (var reader = new StreamReader(file.OpenReadStream(), Encoding.GetEncoding(1251)))
                    {
                        while (reader.Peek() > 0)
                        {
                            string fullLine = await reader.ReadLineAsync();

                            IEntity result = Parse(fullLine, '\t');

                            if (result != null)
                            {
                                entities.Add(result);
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    return null;
                }
                return entities;
            }
            return null;
        }

        /// <summary>
        /// Производит разбиение ФИО на отдельные элементы и делает первую букву у каждого заглавной.
        /// </summary>
        /// <param name="data">Строка для разбора</param>
        /// <returns>Возвращает массив строк срдержащих отдельно Фамилию Имя и Отчество</returns>
        public string[] ParseName(string data)
        {
            string[] fullName = data.ToLower().Split(' ');

            for (int i = 0; i < fullName.Length; i++)
            {
                if (!String.IsNullOrEmpty(fullName[i]))
                    fullName[i] = fullName[i].First().ToString().ToUpper() + fullName[i].Substring(1);
            }
            return fullName;
        }

        /// <summary>
        /// Производит проверку номера телефона удаляя все символы кроме цифр и обрезая длину до 10 знаков с конца.
        /// </summary>
        /// <param name="data">Строка для разбора</param>
        /// <returns>Возвращает номер телефона</returns>
        public string ParsePhoneNumber(string data)
        {
            string phone = new String(data.Where(Char.IsDigit).ToArray());
            if (phone.Length > 10) phone = phone.Substring(phone.Length - 10, 10);
            return phone;
        }

        /// <summary>
        /// Производит парсинг строки в формат DataTime, при неудачном парсинге возвращает минимальне значение даты.
        /// </summary>
        /// <param name="data">Строка для разбора</param>
        /// <returns>Возвращает дату в формате DateTime</returns>
        public DateTime ParseDate(string data)
        {
            DateTime birthDate;
            if (!DateTime.TryParse(data, out birthDate)) birthDate = DateTime.MinValue;
            return birthDate;
        }

        /// <summary>
        /// Производит парсинг строки с e-mail
        /// </summary>
        /// <param name="data">Строка для разбора</param>
        /// <returns>Возвращает e-mail</returns>
        public string ParseEmail(string data)
        {
            return data.ToLower();
        }
    }
}
