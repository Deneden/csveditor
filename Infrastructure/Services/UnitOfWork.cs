﻿using System;
using Domain.Interfaces;
using Infrastructure.Context;
using Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Services
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DbContext _context;

        /// <summary>
        /// Коструктор класса
        /// </summary>
        /// <param name="context">Контекст базы данных</param>
        public UnitOfWork(ApplicationContext context)
        {
            WorksheetQuery = new WorksheetLinqFactory(context);
            _context = context;        
        }

        /// <summary>
        /// Возвращает фабрику запросов для сущности Worksheet.
        /// </summary>
        public IWorksheetLinqFactory WorksheetQuery { get; }

        /// <summary>
        /// Сохрняет изменения в базе данных.
        /// </summary>
        public void Save()
        {
            _context.SaveChanges();
        }

        private bool _disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
