﻿using System.Collections.Generic;
using Domain.Interfaces;
using Domain.Models;
using Infrastructure.Context;
using Infrastructure.Linq;

namespace Infrastructure.Services
{
    public class WorksheetLinqFactory : IWorksheetLinqFactory
    {
        private readonly LinqProvider<Worksheet> _provider;

        /// <summary>
        /// Коструктор класса
        /// </summary>
        /// <param name="context">Контекст базы данных</param>
        public WorksheetLinqFactory(ApplicationContext context)
        {
            _provider = new LinqProvider<Worksheet>(context);
        }

        /// <summary>
        /// Возвращает Worksheet по номеру телефона
        /// </summary>
        /// <param name="id">Id сущности</param>
        /// <returns>Сущность Worksheet</returns>
        public Worksheet FindWorksheetByIdQuery(int id)
        {
            return new FindWorksheetByIdQuery(_provider, id).Execute();
        }

        /// <summary>
        /// Возвращает коллекцию Worksheet
        /// </summary>
        /// <returns>Коллекция Worksheet</returns>
        public IEnumerable<Worksheet> GetAllWorksheets()
        {
            return new GetAllWorksheets(_provider).Execute();
        }

        /// <summary>
        /// Возвращает коллекцию сущностей сортированных по критерию order
        /// </summary>
        /// <returns>Коллекция сущностей Worksheet</returns>
        public IEnumerable<Worksheet> GetWorksheetsOrderBy(string order)
        {
            return new GetWorksheetsOrderBy(_provider, order).Execute();
        }

        /// <summary>
        /// Добавляет Worksheet в репозиторий
        /// </summary>
        /// <param name="worksheet">Сущность Worksheet</param>
        public void AddWorksheet(Worksheet worksheet)
        {
            new AddWorksheet(_provider, worksheet).Execute();
        }

        /// <summary>
        /// Возвращает количество сущностей
        /// </summary>
        /// <returns>Количество сущностей</returns>
        public int GetWorksheetsCount()
        {
            return new GetWorksheetsCount(_provider).Execute();
        }

        /// <summary>
        /// Изменяет Worksheet в базе данных
        /// </summary>
        /// <param name="worksheet">Сущность Worksheet</param>
        public void UpdateWorksheet(Worksheet worksheet)
        {
            new UpdateWorksheet(_provider, worksheet).Execute();
        }

        /// <summary>
        /// Удаляет сущность Worksheet
        /// </summary>
        /// <param name="worksheet">Сущность Worksheet</param>
        public void DeleteWorksheet(Worksheet worksheet)
        {
            new DeleteWorksheet(_provider, worksheet).Execute();
        }

        /// <summary>
        /// Проверяет наличие сущности в базе по номеру телефона
        /// </summary>
        /// <returns>True если есть, иначе false</returns>
        public bool IsWorksheetExist(int id)
        {
            return new IsWorksheetExist(_provider, id).Execute();
        }
    }
}
