﻿namespace Infrastructure.Models
{
    //вспомогательный класс для связывания данных из appsettings.json
    public class AppConfig
    {
        /// <summary>
        /// Количество записей на одной странице.
        /// </summary>
        public int PageSize { get; set; }
    }
}
