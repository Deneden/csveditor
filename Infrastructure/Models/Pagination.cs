﻿using System;
using Infrastructure.Interfaces;

namespace Infrastructure.Models
{
    //вспомогательный класс для пагинации
    public class Pagination : IPagination
    {
        /// <summary>
        /// Количество записей всего.
        /// </summary>
        public int TotalItems { get; set; }

        /// <summary>
        /// Количество записей на одной странице.
        /// </summary>
        public int ItemsPerPage { get; set; }

        /// <summary>
        /// Всего страниц.
        /// </summary>
        public int TotalPages => (int) Math.Ceiling((decimal) TotalItems / ItemsPerPage);
    }
}
